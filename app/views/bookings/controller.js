'use strict';

angular.module('dynaBook')
.controller('bookingCtrl', ['$mdDialog', 'bookings', function($mdDialog, bookings) {
    this.bookings = bookings;

    this.showLarge = function(ev) {
        $mdDialog.show({
            controller: DialogController,
            controllerAs: "ctrl",
            templateUrl: 'views/bookings/booking-dialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: true, // Only for -xs, -sm breakpoints.
            locals : {
                message : ev
            }
        })
    };

    function DialogController($scope, $mdDialog, message) {
        this.message = message;

        this.cancel = function() {
            $mdDialog.cancel();
        };
    }
}]);