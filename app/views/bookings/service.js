/**
 * Created by amc0 on 2016-12-11.
 */
angular.module('dynaBook')
    .factory("bookingsService", function($http){
        return {
            getBookings: function(id){
                console.log("Getting specific booking")
                // return $http.get('http://localhost:8080/studio');
                return $http
                    .get("http://localhost:8080/bookings")
                    .then(function successCallback(response) {
                        console.log(response.data);
                        return response.data;
                    }, function errorCallback() {
                        return undefined;
                    });
                // return "Test"
            }
        }
    });