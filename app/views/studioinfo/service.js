/**
 * Created by amc0 on 2016-12-11.
 */
angular.module('dynaBook')
    .factory("studioService", function($http){
    return {
        getStudio: function(){
            console.log("Getting studio");
            // return $http.get('http://localhost:8080/studio');
            return $http
                .get("http://localhost:8080/studio")
                .then(function successCallback(response) {
                    console.log(response.data);
                    return response.data;
                }, function errorCallback() {
                    return undefined;
                });
            // return "Test"
        }
    }
});