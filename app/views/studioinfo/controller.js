'use strict';

angular.module('dynaBook')
.controller('infoCtrl', ['NgMap', 'studio', function(NgMap, studio) {
    this.studio = studio;

    NgMap.getMap().then(function(map) {
        console.log("Center ", map.getCenter());
        console.log('markers', map.markers);
        console.log('shapes', map.shapes);
    });
}]);