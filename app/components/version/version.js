'use strict';

angular.module('dynaBook.version', [
  'dynaBook.version.interpolate-filter',
  'dynaBook.version.version-directive'
])

.value('version', '0.1');
