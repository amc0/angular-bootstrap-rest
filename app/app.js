'use strict';

// Declare app level module which depends on views, and components
angular.module('dynaBook', [
    'ngRoute',
    'ngMaterial',
    'ngMap',
    'dynaBook.version'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider
        .when('/bookings', {
            templateUrl: 'views/bookings/view.html',
            controller: 'bookingCtrl',
            controllerAs: "ctrl",
            resolve: {
                bookings: function (bookingsService) {
                    return bookingsService.getBookings();
                }
            }
        })
        .when('/info', {
            templateUrl: 'views/studioinfo/view.html',
            controller: 'infoCtrl',
            controllerAs: "ctrl",
            resolve: {
                studio: function (studioService) {
                    return studioService.getStudio();
                }
            }
        })
        .otherwise({redirectTo: '/bookings'});
}]);
